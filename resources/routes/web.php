<?php

use kamilplhh\currency_exchange\src\Controllers\ExchangerController;
use Illuminate\Support\Facades\Route;

Route::get('exchange', [ExchangerController::class, 'index']);
