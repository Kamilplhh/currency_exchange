<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Exchanger</title>
    <body bgcolor="#9797B6">
    <script src="https://cdn.tailwindcss.com"></script>
    <livewire:styles />
</head>

<body>
    <div class="flex items-center justify-center h-screen">
        <livewire:ExchangeManager/>
    </div>
    <livewire:scripts />
</body>
</html>