<center>    
    <h2 class="text-xl">
        Currency exchange
    </h2>
    
    <select wire:model="currency1">
        <option value=1>PLN</option>
        @foreach ($currencies as $currency)
            <option value="{{ $currency->exchange_rate }}">{{ $currency->currency_code }}</option>
        @endforeach
    </select>
    to
    <select wire:model="currency2">
        <option value=1>PLN</option>
        @foreach ($currencies as $currency)
            <option value="{{ $currency->exchange_rate }}">{{ $currency->currency_code }}</option>
        @endforeach
    </select>

    <div style="margin: 10px">
        <input wire:model="number" type="number" placeholder="Value" class="w-20"> 
        <br>
        <button wire:click="calc" class="bg-white hover:bg-gray-100 text-gray-800 font-semibold py-1 px-2 m-2 border border-gray-400 rounded shadow">
            Exchange
        </button>  
    </div>

    <h2 class="text-xl">
        @json($result)
    </h2>
      
</center>