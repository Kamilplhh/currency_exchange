<?php

namespace kamilplhh\currency_exchange\src\Repositories;

use kamilplhh\currency_exchange\src\Interfaces\CurrencyRepositoryInterface;
use kamilplhh\currency_exchange\src\Models\Currency;

class CurrencyRepository implements CurrencyRepositoryInterface
{
    public $currency;
    
    public function __construct(Currency $currency)
    {
        $this->currency = $currency;
    }
    public function getAll() 
    {
        return $this->currency::all();
    }

    public function getById($currencyId) 
    {
        return $this->currency::findOrFail($currencyId);
    }

    public function delete($currencyId) 
    {
        $this->currency::destroy($currencyId);
    }

    public function update($code, $rate) 
    {
        return $this->currency::where('currency_code', $code)
                                ->update(['exchange_rate' => $rate]);
    }

    public function insert($data)
    {
        return $this->currency::insert($data);
    }

}