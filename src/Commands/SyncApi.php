<?php
// Might not work then make this as a controller
namespace App\Console\Commands;

use kamilplhh\currency_exchange\src\Repositories\CurrencyRepository;
use Illuminate\Console\Command;
use kamilplhh\currency_exchange\src\Models\Currency;
use kamilplhh\currency_exchange\src\Helpers\Helper;

class SyncApi extends Command
{
    public CurrencyRepository $currencyRepository;

    public function __construct(CurrencyRepository $currencyRepository) 
    {
        $this->currencyRepository = $currencyRepository;
    }

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:api';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $table = Helper::getApi();

        foreach($table as $tab){
            $objects = ($tab['rates']);
            foreach ($objects as $object){
                $name = ($object['currency']);
                $code = ($object['code']);
                $rate = round(($object['mid']),2);
        
                if (Currency::Where('currency_code', $code)->exists() && (date("Y-m-d") != Currency::all('updated_at'))){
                    $this->currencyRepository->update($code, $rate);
                }
        
                else{
                    $data = array('name' => $name, 'currency_code' => $code, 'exchange_rate' => $rate);
                    $this->currencyRepository->insert($data);
                }
            }          
        }                     
    }
}
