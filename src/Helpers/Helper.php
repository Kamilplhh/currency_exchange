<?php

namespace kamilplhh\currency_exchange\src\Helpers;


class Helper {

    public static function getApi() 
    {

        $url = "http://api.nbp.pl/api/exchangerates/tables/a/";
        $table = json_decode(file_get_contents($url), true);
        
        return $table;
    }
}