<?php

namespace kamilplhh\currency_exchange\src\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Console\Scheduling\Schedule;
use kamilplhh\currency_exchange\src\Livewire\ExchangeManager;
use Livewire\Livewire;
use kamilplhh\currency_exchange\src\Interfaces\CurrencyRepositoryInterface;
use kamilplhh\currency_exchange\src\Repositories\CurrencyRepository;

class ExchangerProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/../../resources/routes/web.php');
        $this->loadViewsFrom(__DIR__.'/../../views', 'exchange');
        Livewire::component('ExchangeManager', ExchangeManager::class);

        
        $this->callAfterResolving(Schedule::class, function (Schedule $schedule) {
            $schedule->command('sync:api')->daily();
        });
     

        $this->publishes([
            __DIR__ . '/../../database/migrations/create_currencies_table.php' => database_path('migrations/' . date('Y_m_d_His', time()) . '_create_currencies_table.php'),
            ], 'migrations');

        $this->publishes([
            __DIR__ . '/../Commands/SyncApi.php' => app_path('Console/Commands/SyncApi.php'),
            ], 'command');

    }

    public function register() 
    {
        $this->app->bind('CurrencyRepositoryInterface::class', 'CurrencyRepository::class');
    }
}