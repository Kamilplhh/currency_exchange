<?php

namespace kamilplhh\currency_exchange\src\Livewire;

use Livewire\Component;
use kamilplhh\currency_exchange\src\Models\Currency;

class ExchangeManager extends Component
{
    public $currencies;
    public $currency1 = 1;
    public $currency2 = 1;
    public $number;
    public $result = 'Wynik';
    public $first;
    public $second;

    public function render()
    {
        return view('exchange::livewire.exchange-manager');
    }

    public function mount()
    {
        $this->currencies = Currency::all();
    }

    public function calc()
    {
        $this->result = round(($this->currency1 * $this->number) / $this->currency2 ,2);
    
    }
}