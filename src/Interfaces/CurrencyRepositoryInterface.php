<?php

namespace kamilplhh\currency_exchange\src\Interfaces;

interface CurrencyRepositoryInterface
{
    public function getAll(); 
    public function getById($currencyId);
    public function delete($currencyId);
    public function update($code, $rate);

}